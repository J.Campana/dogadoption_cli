import json

import messages
from db_config import *
from models import User, Dog, Gender, Size


def do_login(user):
    login = session.query(User).filter_by(username=user['username'], password=user['password']).first()

    return login


def list_all_dogs():
    dogs = session.query(Dog).filter_by(user_id=None).all()
    msg = messages.LIST_DOGS

    for dog in dogs:
        msg += dog.__str__() + '\n'

    return msg


def adopt_dog(data):
    dog = session.query(Dog).filter_by(id=int(data['dog_id'])).first()

    # If the dog is already adopted
    if dog.user_id is not None:
        return messages.ADOPT_ERROR

    dog.user_id = int(data['user_id'])

    # Not working for some reason
    # query = update(Dog).where(Dog.id == int(data['dog_id'])).values(user_id=int(data['user_id']))

    try:
        session.merge(dog)
        session.commit()
        return messages.ADOPT_SUCCESS
    except ProgrammingError:
        return messages.ADOPT_ERROR


def insert_dog(dog):
    aux = Dog.json_to_dog(Dog, json.loads(dog))

    if aux.gender == "Male":
        gender = Gender.male
    elif aux.gender == "Female":
        gender = Gender.female
    else:
        gender = Gender.male

    if aux.size == "Pequeño":
        size = Size.small
    elif aux.size == "Mediano":
        size = Size.medium
    elif aux.size == "Grande":
        size = Size.large
    else:
        size = Size.medium

    to_insert = Dog(name=aux.name, age=aux.age, breed=aux.breed, size=size, gender=gender,
                    description=aux.description, user_id=None)

    try:
        session.add(to_insert)
        session.commit()
        return messages.INSERT_SUCCESS
    except ProgrammingError:
        return messages.INSERT_ERROR


def insert_user(user):
    aux = User.json_to_user(User, json.loads(user))

    to_insert = User(username=aux.username, password=aux.password, first_name=aux.first_name,
                     last_name=aux.last_name, phone_number=aux.phone_number, email=aux.email, role=aux.role)

    try:
        session.add(to_insert)
        session.commit()
        return messages.INSERT_SUCCESS
    except ProgrammingError:
        return messages.INSERT_ERROR


def list_all_users():
    users = session.query(User).all()
    msg = messages.LIST_USERS

    for user in users:
        msg += user.__str__() + '\n'

    return msg
