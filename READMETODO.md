# Posibles Mejoras

- Implementación de medidas de seguridad del lado del servidor.
- Implementación de métodos de encriptación de mensajes para el paso seguro de datos de usuario.
- Añadir comprobaciones de datos en el ingreso de nuevos perros y usuarios para evitar queries incorrectas.
- Agregar manejo de errores en los servicios para evitar que el servidor explote.
- Implementar el uso de semáforos para evitar problemas de en la utilización de recursos.