# Instalación

Para el correcto funcionamiento de este sistema se deben llevar a cabo los siguientes pasos.

## Clonar el repositorio

Sea a través de SSH o HTTP debemos tener una copia del proyecto localmente.
```
git clone https://gitlab.com/J.Campana/dogadoption_cli.git
```

## Crear el entorno virtual

Esto creará y activara el entorno virtual en el directorio sobre el que estemos navegando.

  ```sh
$ python3 -m venv .
$ source bin/activate
```

## Instalar los requerimientos del programa
El siguiente comando instalará en el entorno virtual los requerimientos necesarios para el correcto funcionamiento.

  ```sh
$ pip3 install -r requirements.txt
```

## Importar las credenciales necesarias para la DB
Por razones de seguridad para el usuario es necesario que ciertas credenciales no queden visibles en el código, es por esto que se debe crear un archivo local que las contenga:
  ```sh
  $ touch .env
  ```
Un vez creado, deberían de poder ver un archivo llamado **.env**. Al mismo debemos agregarles las siguientes variables: 
  ```
DB_USERNAME=Nombre_de_usuario_de_MySQL
DB_PASS=Contraseña_de_MySQL
DB_NAME=Nombre_de_schema_de_MySQL_a_utilizar
  ```

## Crear las tablas en la DB

Correr el siguiente comando creará las tablas necesarias en la base de datos:
  ```sh
  $ python3 models.py -c
  ```
Si se desea llenar las tablas con datos de prueba se puede correr el comando con el siguiente comando:
  ```sh
  $ python3 models.py -p
  ```
O realizar ambos al mismo tiempo:
  ```sh
  $ python3 models.py -c -p
  ```

## Referirse al archivo Readme.md para ver el funcionamiento del sistema

