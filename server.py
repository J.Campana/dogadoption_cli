import getopt
import socket
import sys
import threading
from server_service import *


def new_client(clientsocket):
    logged = False
    print(messages.SV_THREAD, threading.get_ident())
    ip, host = clientsocket.getpeername()
    print(messages.SV_CONNECTION, ip, host)

    while True:
        # Data received from client

        client_opt = clientsocket.recv(1024)
        print(client_opt.decode())

        if client_opt.decode() == '-g' or client_opt.decode() == '--login' and logged is False:
            user = clientsocket.recv(1024).decode()
            login = do_login(json.loads(user))
            if login:
                logged = True
                logged_user = login
                clientsocket.send(json.dumps(login.user_to_json()).encode())
            else:
                clientsocket.send(messages.LOGIN_ERROR.encode())

        elif client_opt.decode() == '-n' or client_opt.decode() == '--new-user' and logged is True:
            user = clientsocket.recv(1024).decode()
            clientsocket.send(insert_user(user).encode())

        elif client_opt.decode() == '-u' or client_opt.decode() == '--list-users' and logged is True:
            users = list_all_users()
            clientsocket.send(users.encode())

        elif client_opt.decode() == '-i' or client_opt.decode() == '--insert' and logged is True:
            dog = clientsocket.recv(1024).decode()
            clientsocket.send(insert_dog(dog).encode())

        elif client_opt.decode() == '-l' or client_opt.decode() == '--list' and logged is True:
            dogs = list_all_dogs()
            clientsocket.send(dogs.encode())

        elif client_opt.decode() == '-a' or client_opt.decode() == '--adopt' and logged is True:
            dogs = list_all_dogs()
            clientsocket.send(dogs.encode())
            data = json.loads(clientsocket.recv(1024).decode())
            print(data)
            if data != messages.ADOPT_ATTEMPT:
                clientsocket.send(adopt_dog(data).encode())

        elif client_opt.decode() == '-q' or client_opt.decode() == '--logout' and logged is True:
            logged = False

        elif client_opt.decode() == '-e' or client_opt.decode() == '--exit':
            break

        elif not client_opt:
            break

        else:
            print(messages.WRONG_OPTION)

    # connection closed
    print(messages.SCK_CLOSED, ip, host)
    clientsocket.close()


def main():
    port = 8080
    host = ''

    (opt, arg) = getopt.getopt(sys.argv[1:], 'p:')

    for (op, ar) in opt:

        if op == '-p':
            port = int(ar)

    socket_list = []

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((host, port))
        s.listen(5)

    except socket.error or PermissionError:
        print(messages.SCKT_ERROR)
        sys.exit()

    print(messages.SCKT_CREATED)

    print(messages.SV_START)

    print(messages.SV_WAITING)

    # A forever loop until client wants to exit
    while True:
        threads_list = list()
        try:
            # Establish connection with client
            c, addr = s.accept()
            client_data = c, addr
            socket_list.append(client_data)

            th = threading.Thread(target=new_client, args=(c,))

            threads_list.append(th)
            th.daemon = True
            th.start()

            for thread in threads_list:
                thread.join(0.5)

        except KeyboardInterrupt:
            print(messages.KYBRD_INTERRUPT)
            sys.exit()

        except EOFError:
            print(messages.EOFE)
            sys.exit()
    s.close()


if __name__ == '__main__':
    main()
