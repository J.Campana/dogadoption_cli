import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import ProgrammingError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import CreateSchema
from dotenv import load_dotenv

"""
File that has the ORM SQLAlchemy configurations.
It also has got the url to the database configured using environment variables.
"""

load_dotenv()

dbUrl = 'mysql+pymysql://'+os.getenv('DB_USERNAME')+':'+os.getenv('DB_PASS')+'@localhost'

engine = create_engine(dbUrl)

"""
Create schema if it doesn't exist.
"""

try:
    engine.execute(CreateSchema(os.getenv('DB_NAME')))
except ProgrammingError:
    pass

engine.execute('USE '+os.getenv('DB_NAME'))

Session = sessionmaker(bind=engine)

Base = declarative_base(bind=engine)

session = Session()
