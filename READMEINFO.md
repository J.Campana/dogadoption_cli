# Readme Info
En este archivo se justificará la elección de las diferentes herramientas, tecnologías, módulos entre otros desde una perspectiva teórico-práctica.

## Base de Datos

Se utilizó una base de datos relacional, en este caso MySQL por su facilidad de uso con respecto a otras como pueden ser PostgreSQL y por experiencias previas utilizando este motor de base de datos.
Para el modelo de base de datos que se está utilizando en este programa, no es justificable el uso de bases de datos no relacionales las cuales sobresalen más que nada en el almacenamiento de documentos.

## ORM

Los ORM son una capa de software entre la base de datos real y las aplicaciones utilizadas para la gestión de la base de datos. Estos permiten la utilización de acciones comunes a cualquier base de datos (Como son las CRUD), asi como también la creación de queries más complejas, sin necesidad de conocer las especificaciones de la base de datos a utilizar.

## Sockets
El porqué de la implementación de estos sockets, es que principalmente utilizan el protocolo de transporte TCP (Transfer Control Protocol), el cuál se caracteríza por garantizar la entrega de los paquetes o en este caso denominado segmentos a los extremos de la conexión, es decir, a los clientes, como al servidor. Un socket básicamente esta formado por una dirección IP y un determinado número de puerto. Esto permite la multiplxeación del sistema, es decir, poder conectar multiples clientes a una misma dirección de servidor, cosa que el protocolo IP no puede realizar de por sí solo.

- **socket.AF_INET**:
Permite la comunicación entre el servidor y los clientes.
AF_INET Está relacionado con el protocolo IPV4. Cuando utilice sockets AF_INET, vincule el servidor a "localhost".

- **socket.SOCK_STREAM**:
STREAM está relacionado con el protocolo TCP.

## Threading
Para poder multiplexar las conexiones el servidor, se estableció como requisito que el hilo principal de server.py inicialice uno nuevo por cada cliente que se conecte. A su vez, el módulo threading de python posee mecanismos para controlar el funcionamiento de estos hilos.
