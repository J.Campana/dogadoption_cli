"""
This python file contains all the messages or strings that are shown in
client terminals as well as server's terminal.
"""

SV_START = "\nServer starting...\n"

SV_WAITING = "\nServer waiting for clients \n"

SV_CONNECTION = "\n>>Server got connection from\n"

SV_RECV_ID = "\n>>Server recieved Id: \n"

SV_THREAD = "\nInitializing thread 'ID':\n"

SCKT_ERROR = "\nError during creating the socket. Stopping execution\n"

SCKT_CREATED = "\nSocket succesfully created!\n"

SCK_CLOSED = "\nSocket closed! Client disconected: \n"

CLIENT_MENU_NO_LOGIN = """"-----Menu-----

Comandos disponibles:

--login / -g Iniciar Sesión
--exit / -e  Desconectar del Cliente
"""

CLIENT_MENU_GUEST = """
-----------------Menu de Adopción-----------------

Comandos disponibles:

--insert / -i    Insertar nuevo perro al sistema          
--list / -l      Listar todos los perror            
--adopt / -a     Adoptar un perro 
--clear / -c     Limpiar la terminal 
--logout / -q    Cerrar Sesión           
--exit / -e      Desconectar del Cliente
"""

CLIENT_MENU_ADMIN = """
-----------------Menu de Adopción-----------------

Comandos disponibles :

--new-user / -n   Crear nuevo usuario
--list-users / -u Listar los usuarios existentes
--insert / -i     Insertar nuevo perro al sistema          
--list / -l       Listar todos los perror            
--adopt / -a      Adoptar un perro 
--clear / -c      Limpiar la terminal 
--logout / -q     Cerrar Sesión           
--exit / -e       Desconectar del Cliente
"""

LIST_DOGS = "\n\nPerros en Adopción \n\n"

LIST_USERS = "\n\nUsuarios registrados \n\n"

OPT_EXIT = ">>Shutting down the conection, bye!"

CLIENT_CLEARED = "A client cleared its own terminal"

CLIENT_USER = "Usuario: "

CLIENT_PASSWORD = "Password:"

KYBRD_INTERRUPT = "\nInterrupción por teclado"

EOFE = "\n¡EOFE!"

SUCCESS = "SUCCESS"

ERROR = "ERROR"

LOGIN = "\nInicio de sesión por parte de: %r"

LOGIN_ERROR = "\nUsuario o contraseña incorrectos. Por favor intente de nuevo.\n"

WRONG_OPTION = "\nNo existe tal opción. Por favor intente de nuevo.\n"

LOGOUT = "\nSe ha cerrado su sesión. Para volver a utilizar el sistema inicie sesion nuevamente.\n"

CONTINUE = "\nPresione cualquier boton para continuar..."

CONTINUE_MENU = "\nPresione cualquier boton para volver al menu principal..."

ADOPT = "\nSeleccione el id del perro que quiere adoptar: "

ADOPT_ATTEMPT = "\nUn cliente intento adoptar un perro."

ADOPT_QUESTION = "\n¿Esta seguro de que quiere adoptar el perro con ID: %r ? (y/n)\n"

ADOPT_SUCCESS = "\nFelicitaciones! Usted a adoptado un perro\n"

ADOPT_ERROR = "\nLo sentimos pero ha ocurrido un error durante el proceso de adopción. Por favor intente de nuevo.\n"

# Messages for dog insertion

INSERT_DOG = "\nA continuación le pediremos los datos del perro que desea ingresar en el sistema.\n"

NAME = "Nombre: "

AGE = "Edad en años: "

BREED = "Raza (Opcional): "

SIZE = """Ingrese el número correspondiente al tamaño del perro: 
1) Pequeño
2) Mediano
3) Grande

"""

GENDER = """Ingrese el número correspondiente al género del perro:
1) Macho
2) Hembra

"""

DESCRIPTION = "Ingrese una breve descripción (Opcional):"

INSERT_USER = "\nA continuación le pediremos los datos del usuario que desea ingresar en el sistema.\n"

USERNAME = "Nombre de usuario: "

PASSWORD = "Contraseña: "

EMAIL = "Correo Electrónico: "

PHONE_NUMBER = "Número de teléfono: "

FIRST_NAME = "Nombre: "

LAST_NAME = "Apellido: "

ROLE = """Ingrese el número correspondiente al rol del usuario:
1) User
2) Admin

"""

INSERT_SUCCESS = "Ha ingresado el perro con éxito!"

INSERT_ERROR = "Ha ocurrido un error al insertar el perro. Por favor, intente de nuevo."
