import getpass
import messages
import json
from models import User, Dog, Size, Gender, Role


def login_data():
    username = input(messages.CLIENT_USER)
    password = getpass.getpass()
    user = {"username": username, "password": password}
    return json.dumps(user)


def dog_adopt_data(dog_id, user_id):
    data = {"dog_id": dog_id, "user_id": user_id}
    return json.dumps(data)


def dog_data():
    print(messages.INSERT_DOG)
    name = input(messages.NAME)
    age = input(messages.AGE)
    breed = input(messages.BREED)
    size = input(messages.SIZE)

    if size == 1:
        size = Size.small
    elif size == 2:
        size = Size.medium
    elif size == 3:
        size = Size.large
    else:
        size = Size.medium

    gender = input(messages.GENDER)

    if gender == 1:
        gender = Gender.male
    elif gender == 2:
        gender = Gender.female
    else:
        gender = Gender.male

    description = input(messages.DESCRIPTION)

    dog = Dog(name, age, breed, size, gender, description, None).dog_to_json()

    return json.dumps(dog)


def user_data():
    print(messages.INSERT_USER)
    username = input(messages.USERNAME)
    password = input(messages.PASSWORD)
    first_name = input(messages.FIRST_NAME)
    last_name = input(messages.LAST_NAME)
    phone_number = input(messages.PHONE_NUMBER)
    email = input(messages.EMAIL)
    role = input(messages.ROLE)

    if role == 1:
        role = Role.user
    elif role == 2:
        role = Role.admin
    else:
        role = Role.user

    user = User(username, password, first_name, last_name, phone_number, email, role).user_to_json()

    return json.dumps(user)
