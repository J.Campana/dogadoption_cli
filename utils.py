
def clear_terminal():
    """
    Function that clears a terminal screen.
    """
    print('\n' * 100)
