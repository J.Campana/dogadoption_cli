# DogAdoption CLI
Este es un sistema de adopcion de perros por linea de comandos desarrollado en Python.

## Requerimientos

```
Python 3.8+
Pip 22+
MySQL 8+
```

## Comienzo

1. Crear el servidor utilizando el siguiente comando:


```
python3 server.py -p port_number
```

2. Crear un cliente se utiliza el siguiente comando:


```
python3 client.py -p port_number -h IP_address
```

## El Sistema
Lo primero que nos vamos a encontrar cuando corremos el cliente, es que este nos va a pedir que iniciemos sesión:

```
--login / -g Iniciar Sesión
```

Ingresando el comando *-g* o *--login* podremos iniciar sesión en el sistema.
Por defecto ya se encuentran creados dos usuarios, uno con rol de ADMIN y otro con rol de GUEST.

```
Usuario ADMIN

    Usuario: admin
    Pass: admin

Usuario GUEST

    Usuario: guest
    Pass: guest
```


## Opciones dentro del sistema
Una vez iniciada la sesión, se presentara el siguiente menu con opciones para los usuarios:
 
```
-----Menu de Adopción-----

Comandos disponibles :

--new-user / -n  Crear nuevo usuario
--insert / -i    Insertar nuevo perro al sistema          
--list / -l      Listar todos los perror            
--adopt / -a     Adoptar un perro 
--clear / -c     Limpiar la terminal 
--logout / -q    Cerrar Sesión           
--exit / -e      Desconectar del Cliente
```

Se puede acceder a cualquiera de estas opciones a traves de su expresion completar o su abreviatura. Como por ejemplo, podriamos salir del sistema utlizando *--exit* o  *-e*.
### Nuevo Usuario
Esta opción le permite a un usuario con el rol de ADMIN crear un nuevo usuario guest en el sistema.
###Insertar Perro
Esto comenzará un proceso de pedido de datos sobre el can en cuestión para registrarlo en el sistema.
### Listar Perros
Esta opción nos traerá una lista con todos los perros que no cuenten con un user_id, es decir, que no hayan sido adoptados aun.
### Adoptar un Perro
Nos permite seleccionar un perro de la lista de aquellos que nos han sido adoptados aun y asociarlo a nuestro usuario.
### Limpiar la terminal
Nos da la opción de limpiar la terminal manualmente.
### Cerrar Sesión
Devuelve al usuario a la pantalla inicial para salir o volver a iniciar sesión.
### Salir del sistema
Cierra el cliente y sale del sistema.


