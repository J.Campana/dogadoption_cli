import socket
import getopt
import sys
from client_service import *
from utils import *
from models import User


def main():
    # local host IP '127.0.0.1'
    host = '127.0.0.1'
    # Define the port on which you want to connect
    port = 8080
    # Start with an empty user
    session = None

    (opt, arg) = getopt.getopt(sys.argv[1:], 'a:p:')

    for (op, ar) in opt:
        if op == '-a':
            host = str(ar)
        elif op == '-p':
            port = int(ar)

    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((host, port))
        print(messages.SCKT_CREATED)

    except socket.error or ConnectionRefusedError:
        print(messages.SCKT_ERROR)
        sys.exit()

    while True:
        try:
            # Check if user has logged into his account
            if session is None:
                print(messages.CLIENT_MENU_NO_LOGIN)
                option = input("Option: ")

                if option in ['-g', '--login']:
                    client.send(option.encode())
                    user = login_data()
                    client.send(user.encode())
                    response = client.recv(1024).decode()
                    if response == messages.LOGIN_ERROR:
                        print(messages.LOGIN_ERROR)
                    else:
                        session = User.json_to_user(User, json.loads(response))
                elif option in ['-e', '--exit']:
                    client.send(option.encode())
                    break
                else:
                    print(messages.WRONG_OPTION)
            else:
                # If he is logged in, show menu according to User.role
                print(messages.CLIENT_MENU_ADMIN) if session.role == 'Admin' else print(messages.CLIENT_MENU_GUEST)
                option = input("Option: ")

                client.send(option.encode())

                if session.role == "Admin":
                    if option in ['-n', '--new-user']:
                        user = user_data()
                        client.send(user.encode())
                        print(client.recv(1024).decode())

                    if option in ['-u', '--list-users']:
                        print(client.recv(1024).decode())
                        input(messages.CONTINUE_MENU)

                if option in ['-i', '--insert']:
                    dog = dog_data()
                    client.send(dog.encode())
                    print(client.recv(1024).decode())

                if option in ['-l', '--list']:
                    print(client.recv(1024).decode())
                    input(messages.CONTINUE_MENU)

                elif option in ['-a', '--adopt']:
                    print(client.recv(1024).decode())
                    dog_id = input(messages.ADOPT)
                    data = dog_adopt_data(dog_id, session.id)
                    print(messages.ADOPT_QUESTION % dog_id)
                    if input() in ['y', 'Y']:
                        client.send(data.encode())
                        print(client.recv(1024).decode())
                        print(messages.CONTINUE_MENU)
                    else:
                        client.send(messages.ADOPT_ATTEMPT)
                        print(messages.CONTINUE_MENU)

                elif option in ['-c', '--clear']:
                    clear_terminal()

                elif option in ['-q', '--logout']:
                    session = None
                    print(messages.LOGOUT)

                elif option in ['-e', '--exit']:
                    break

                else:
                    print(messages.WRONG_OPTION)

        except KeyboardInterrupt:
            print(messages.KYBRD_INTERRUPT)
            sys.exit()
        except EOFError:
            print(messages.EOFE)
            sys.exit()

    input(messages.CONTINUE)
    print(messages.OPT_EXIT)
    # close the connection
    client.close()


if __name__ == '__main__':
    main()
