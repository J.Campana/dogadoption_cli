import enum
import getopt
import sys

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import *
from db_config import *


class Size(enum.Enum):
    small = "Pequeño"
    medium = "Mediano"
    large = "Grande"

    def __str__(self):
        return str(self.value)


class Gender(enum.Enum):
    male = "Macho"
    female = "Hembra"

    def __str__(self):
        return str(self.value)


class Role(enum.Enum):
    admin = "Admin"
    user = "User"

    def __str__(self):
        return str(self.value)


class Dog(Base):
    __tablename__ = "dogs"
    id = Column(Integer, primary_key=True)
    name = Column(String(60))
    age = Column(Integer)
    breed = Column(String(60))
    size = Column(Enum(Size))
    gender = Column(Enum(Gender))
    description = Column(Text)
    user_id = Column(Integer, ForeignKey('users.id'), default=None)

    def __str__(self):
        """
        Dog as string.
        @return: returns a string of the dog with its attributes
        """
        return str(
            str(self.id) + ')' + self.name + ': Edad ' + str(
                self.age) + ' Años, Raza ' + self.breed + ', Tamaño ' + self.size.__str__() + ', ' + self.gender.__str__())

    def __repr__(self):
        """
        Getter for Dog.
        @return: returns a dog with its attributes
        """
        return '<Dog: %r %r %r %r %r %r %r %r >' % (
            self.id, self.name, self.age, self.breed, self.size, self.gender, self.description, self.user_id)

    def __init__(self, name, age, breed, size, gender, description, user_id):
        """
        Constructor of a Dog object
        @param name
        @param age
        @param breed
        @param gender
        @param description
        @param user_id
        """
        self.name = name
        self.age = age
        self.breed = breed
        self.size = size
        self.gender = gender
        self.description = description
        self.user_id = user_id

    def dog_to_json(self):
        dogjson = {
            'id': self.id,
            'name': self.name,
            'age': self.age,
            'breed': self.breed,
            'size': self.size.value,
            'gender': self.gender.value,
            'description': self.description,
            'user_id': self.user_id
        }
        return dogjson

    def json_to_dog(self, userjson):
        self.id = userjson['id']
        self.name = userjson['name']
        self.age = userjson['age']
        self.breed = userjson['breed']
        self.size = userjson['size']
        self.gender = userjson['gender']
        self.description = userjson['description']
        self.user_id = userjson['user_id']
        return self


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    username = Column(String(60))
    password = Column(String(60))
    first_name = Column(String(60))
    last_name = Column(String(60))
    phone_number = Column(String(20))
    email = Column(String(255))
    role = Column(Enum(Role))

    def __str__(self):
        """
        Dog as string.
        @return: returns a string of the dog with its attributes
        """
        return str(
            str(self.id) + ')' + self.username + ': Role ' + self.role.__str__())

    def __repr__(self):
        """
        Getter for User.
        @return: returns a user with its attributes
        """
        return '<User: %r %r %r %r %r %r %r >' % (
            self.id, self.username, self.first_name, self.last_name, self.phone_number, self.email, self.role)

    def __init__(self, username, password, first_name, last_name, phone_number, email, role):
        """
        Constructor of a User object
        @param username
        @param password
        @param first_name
        @param last_name
        @param phone_number
        @param email
        @param role
        """

        self.username = username
        self.password = password
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.email = email
        self.role = role

    def user_to_json(self):
        userjson = {
            'id': self.id,
            'username': self.username,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'phone_number': self.phone_number,
            'email': self.email,
            'role': self.role.value
        }
        return userjson

    def json_to_user(self, userjson):
        self.id = userjson['id']
        self.username = userjson['username']
        self.first_name = userjson['first_name']
        self.last_name = userjson['last_name']
        self.phone_number = userjson['phone_number']
        self.email = userjson['email']
        self.role = userjson['role']
        return self


def create_models_to_db():
    Base.metadata.create_all()


def populate_db():
    objects = [User("admin", "admin", "Julian", "Admin", "123456787", "campanajulian@gmail.com", Role.admin),
               User("guest", "guest", "Julian", "Guest", "123456787", "campanajulian@gmail.com", Role.user),
               Dog("Firulais", 4, "Terrier", Size.small, Gender.male, "Es un muy buen perro. Ta vacunado.", None),
               Dog("Roberta", 2, "Bulldog", Size.large, Gender.female, "Es un muy buen perro. Ta vacunado.", None),
               Dog("Choco", 3, "Otra Raza", Size.small, Gender.male, "Es un muy buen perro. Ta vacunado.", None),
               Dog("Este Perro", 8, "No estoy seguro", Size.small, Gender.male, "Es un muy buen perro. Ta vacunado.", None),
               Dog("Ya no se", 12, "Chihuahua?", Size.medium, Gender.female, "Es un muy buen perro. Ta vacunado.", None)]

    session.add_all(objects)

    try:
        session.commit()

    except SQLAlchemyError as e:
        session.rollback()
        print(e)


def main():
    (opt, arg) = getopt.getopt(sys.argv[1:], 'p:c:')

    for (op, ar) in opt:
        if op == '-p':
            populate_db()
        if op == '-c':
            create_models_to_db()


if __name__ == '__main__':
    main()
